//
//  KeyboardUtil.swift
//  BaseApp
//
//  Created by Štěpán Klouček on 27/07/2020.
//  Copyright © 2020 SYNETECH. All rights reserved.
//

#if canImport(UIKit)
import RxSwift
import UIKit

public class KeyboardUtil {

    /// A listener for keyboard changes implemented via RxSwift
    ///
    /// - Returns: Observable (height: CGFloat, animationDuration: Double) that executes when keyboard height changes.
    public static var keyboardHeight: Observable<(height: CGFloat, animationDuration: Double)> {
        return Observable
            .from([getHeightAndAnimation(for: UIResponder.keyboardWillShowNotification),
                   getHeightAndAnimation(for: UIResponder.keyboardWillHideNotification)])
            .merge()
    }

    private static func getHeightAndAnimation(for notification: Notification.Name)
        -> Observable<(height: CGFloat, animationDuration: Double)> {
        return NotificationCenter
            .default
            .rx
            .notification(notification)
            .mapToHeightAndAnimationDuration()
    }
}

fileprivate extension Observable where Element == Notification {

    func mapToHeightAndAnimationDuration() -> Observable<(height: CGFloat, animationDuration: Double)> {
        return self.map { notification -> (CGFloat, Double) in
            return (self.getHeight(notification), self.getAnimationDuration(notification))
        }
    }

    private func getHeight(_ notification: Notification) -> CGFloat {
        if notification.name == UIResponder.keyboardWillHideNotification { return 0 }
        return (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height ?? 0
    }

    private func getAnimationDuration(_ notification: Notification) -> Double {
        return notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double ?? 0
    }
}
#endif
