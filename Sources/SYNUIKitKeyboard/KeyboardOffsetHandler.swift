//
//  KeyboardOffsetHandler.swift
//  BaseApp
//
//  Created by Štěpán Klouček on 27/07/2020.
//  Copyright © 2020 SYNETECH. All rights reserved.
//

#if canImport(UIKit)
import RxCocoa
import RxSwift
import UIKit

public class KeyboardOffsetHandler {

    // MARK: - Properties
    private let viewContainer: UIView
    private var viewsToRemainVisible: [UIView] = []

    private var keyboardYCoordinate: CGFloat = 0
    private var lastKeyboardHeight: CGFloat = 0

    private let shouldResetPositions = BehaviorRelay<Bool>(value: false)

    private var activeTextField: UITextField?
    private var isActiveTextFieldLast: Bool?

    private var followingFieldMargin: CGFloat = 64
    private var addFollowingFieldMarginOnLast: Bool = false
    private var bottomMargin: CGFloat = 16

    private let disposeBag = DisposeBag()

    // MARK: - Init
    /// Creates handler for ensuring that active text fields will be visible even when the keyboard appears.
    /// The instance of this class must be stored within your view controller (or somewhere else).
    /// - Parameter viewContainer: View which wraps the text fields or the whole screen.
    /// This view will be moved with the text fields. It can't be the view controller view.
    /// It can't contain the other views to be visibile since they'll be tranformed twice then.
    public init(viewContainer: UIView) {
        self.viewContainer = viewContainer
        bindKeyboardYCoordinate()
        bindPositionsReset()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Setup
    private func bindKeyboardYCoordinate() {
        KeyboardUtil.keyboardHeight
            .do(onNext: { [weak self] height, animationDuration in
                self?.keyboardYCoordinate = UIScreen.main.bounds.height - height
                if self?.lastKeyboardHeight ?? 0 == 0 {
                    self?.makeOtherViewsVisibile(keyboardHeight: height, animationDuration: animationDuration)
                }
            })
            // This makes sure that the views wount bounce when switching between two text fields
            .debounce(.milliseconds(5), scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] height, animationDuration in
                self?.makeActiveTextFieldVisible()
                if self?.lastKeyboardHeight ?? 0 != 0 {
                    self?.makeOtherViewsVisibile(keyboardHeight: height, animationDuration: animationDuration)
                }
                self?.lastKeyboardHeight = height
            })
            .disposed(by: disposeBag)
    }

    private func bindPositionsReset() {
        shouldResetPositions
            .debounce(.milliseconds(5), scheduler: MainScheduler.instance)
            .filter { $0 }
            .subscribe(onNext: { [weak self] _ in
                self?.resetPositions()
            })
            .disposed(by: disposeBag)
    }
}

// MARK: - Interactions
public extension KeyboardOffsetHandler {

    /// This will make sure that all the passed text fields (just the active one at a time)
    /// and views will be visibile when keyboard appears.
    /// - Parameters:
    ///   - textFields: All text fields that should be visible. The active one will be always visibie even
    ///   with a little offset, which should make sure that the following text field is also visibile (at least partly).
    ///   The offset is ignored for the last text field in the array.
    ///   - forOtherViews: This views will always appear right above the keyboard
    ///   and below the active text field. The view which is layouted at the most bottom
    ///   will be used to calculate the offset of all the views,  so their constraints can remain.
    ///   - followingFieldMargin: It declares how much the text field should offset to make
    ///   the following field visible. Default is `64`
    ///   - addFollowingFieldMarginOnLast: If true, than even the last field will keep the margin under it.
    ///   Default is `false`
    ///   - bottomMargin: Margin from keyboard. Default is `16`.
    func ensureVisibility(forFields textFields: [UITextField], forOtherViews views: [UIView] = [],
                          followingFieldMargin: CGFloat? = nil, addFollowingFieldMarginOnLast: Bool? = nil,
                          bottomMargin: CGFloat? = nil) {
        bindTextFieldsEvent(textFields)
        self.viewsToRemainVisible = views
        if let followingFieldMargin = followingFieldMargin {
            self.followingFieldMargin = followingFieldMargin
        }
        if let addFollowingFieldMarginOnLast = addFollowingFieldMarginOnLast {
            self.addFollowingFieldMarginOnLast = addFollowingFieldMarginOnLast
        }
        if let bottomMargin = bottomMargin {
            self.bottomMargin = bottomMargin
        }
    }
}

// MARK: - Text fields events binding
private extension KeyboardOffsetHandler {

    func bindTextFieldsEvent(_ textFields: [UITextField]) {
        for i in 0..<textFields.count {
            bindEditingBegin(for: textFields[i], isLast: i == textFields.count - 1)
            bindEditingEnd(for: textFields[i])
        }
    }

    func bindEditingBegin(for textField: UITextField, isLast: Bool) {
        textField.rx.controlEvent(.editingDidBegin)
            .do(onNext: { [weak self] in self?.shouldResetPositions.accept(false) })
            // wait until keyboard height is updated (for iPhone 7 and higher)
            .delay(.milliseconds(5), scheduler: MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] in
                self?.activeTextField = textField
                self?.isActiveTextFieldLast = isLast
                // Not making active field visible if keyboardYCoordinate and lastKeyboardHeight are not known
                if self?.lastKeyboardHeight ?? 0 != 0 && self?.keyboardYCoordinate ?? 0 != 0 {
                    self?.makeActiveTextFieldVisible()
                }
            })
            .disposed(by: disposeBag)
    }

    func bindEditingEnd(for textField: UITextField) {
        textField.rx.controlEvent(.editingDidEnd)
            .subscribe(onNext: { [weak self] in
                self?.shouldResetPositions.accept(true)
            })
            .disposed(by: disposeBag)
    }
}

// MARK: - Moving views
private extension KeyboardOffsetHandler {

    func makeActiveTextFieldVisible() {
        guard let textField = activeTextField, let isLast = isActiveTextFieldLast else { return }
        var fieldMaxY = textField.getGlobalMaxY() - viewContainer.transform.ty
        if !isLast || addFollowingFieldMarginOnLast {
            fieldMaxY += followingFieldMargin
        }
        var viewsOffset: CGFloat = calculateOtherViewsOffset()
        if viewsOffset > 0 {
            viewsOffset += bottomMargin * 2
        }
        let targetMaxY = fieldMaxY + viewsOffset
        if targetMaxY > keyboardYCoordinate {
            let verticalOffset = targetMaxY - keyboardYCoordinate
            animate(viewContainer, verticalOffset: verticalOffset)
        }
    }

    func calculateOtherViewsOffset() -> CGFloat {
        let minY = viewsToRemainVisible.map { $0.frame.minY }.min() ?? 0
        let maxY = viewsToRemainVisible.map { $0.frame.maxY }.max() ?? 0
        return maxY - minY
    }

    func makeOtherViewsVisibile(keyboardHeight: CGFloat, animationDuration: Double) {
        guard keyboardHeight != 0 else { return }
        let maxY = viewsToRemainVisible.map { $0.getGlobalMaxY() - $0.transform.ty }.max() ?? UIScreen.main.bounds.maxY
        let defaultViewsOffset = UIScreen.main.bounds.maxY - maxY
        viewsToRemainVisible.forEach {
            animate($0, verticalOffset: keyboardHeight + bottomMargin - defaultViewsOffset,
                    duration: animationDuration)
        }
    }

    func resetPositions() {
        viewsToRemainVisible.forEach { animate($0, verticalOffset: nil) }
        animate(viewContainer, verticalOffset: nil)
    }

    func animate(_ view: UIView, verticalOffset: CGFloat?, duration: TimeInterval = 0.25) {
        UIView.animate(withDuration: duration, animations: {
            if let verticalOffset = verticalOffset {
                view.transform = CGAffineTransform.identity.translatedBy(x: 0, y: -verticalOffset)
            } else {
                view.transform = .identity
            }
        })
    }
}

// MARK: - Global max Y
private extension UIView {

    func getGlobalMaxY() -> CGFloat {
        let rootView = UIApplication.shared.keyWindow?.rootViewController?.view
        return (self.superview?.convert(self.frame, to: rootView).maxY ?? 0)
    }
}
#endif
