//
//  UIView+IsShown.swift
//  
//
//  Created by Lukáš Růžička on 09/11/2020.
//

#if canImport(UIKit)
import UIKit

public extension UIView {

    var isShown: Bool {
        get { return !isHidden }
        set { isHidden = !newValue }
    }
}
#endif
