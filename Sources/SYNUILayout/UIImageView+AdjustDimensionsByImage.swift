//
//  UIImageView+AdjustDimensionsByImage.swift
//  
//
//  Created by Lukáš Růžička on 08.01.2021.
//

#if canImport(UIKit)
import UIKit

public extension UIImageView {

    /// Sets height according to frame width and aspect ratio of the assigned image
    func adjustHeightByWidthAndImageRatio() {
        guard frame.width != 0, let image = image else { return }
        let heightRatio = image.size.height / image.size.width
        adjustHeight(heightRatio * frame.width)
    }

    /// Sets width according to frame height and aspect ratio of the assigned image
    func adjustWidthByHeightAndImageRatio() {
        guard frame.height != 0, let image = image else { return }
        let widthRatio = image.size.width / image.size.height
        adjustWidth(widthRatio * frame.height)
    }
}
#endif
