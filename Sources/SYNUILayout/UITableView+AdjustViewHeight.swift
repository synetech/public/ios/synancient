//
//  UITableView+AdjustViewHeight.swift
//  
//
//  Created by Lukáš Růžička on 08.01.2021.
//

#if canImport(UIKit)
import UIKit

public extension UITableView {

    /// Call this from `layoutSubviews` to dynamically update table header height.
    func adjustTableHeaderHeight() {
        guard let headerView = tableHeaderView else {
            return
        }
        let width = bounds.size.width
        let size = headerView.systemLayoutSizeFitting(
            CGSize(width: width, height: UIView.layoutFittingCompressedSize.height))
        if headerView.frame.size.height != size.height {
            headerView.frame.size.height = size.height
            tableHeaderView = headerView
        }
    }

    /// Call this from `layoutSubviews` to dynamically update table footer height.
    func adjustTableFooterHeight() {
        guard let footerView = tableFooterView else {
            return
        }
        let width = bounds.size.width
        let size = footerView.systemLayoutSizeFitting(
            CGSize(width: width, height: UIView.layoutFittingCompressedSize.height))
        if footerView.frame.size.height != size.height {
            footerView.frame.size.height = size.height
            tableFooterView = footerView
        }
    }
}

#endif
