//
//  UIView+AdjustDimensions.swift
//  
//
//  Created by Lukáš Růžička on 09/11/2020.
//

#if canImport(UIKit)
import Stevia
import UIKit

public extension UIView {

    /// Changes the height constraint of the view. Makes sure that the constraint is set even if it wasn't previously assigned.
    /// - Parameter newHeight: The new height constraint to be set.
    func adjustHeight(_ newHeight: CGFloat) {
        if heightConstraint == nil {
            height(newHeight)
        } else {
            heightConstraint?.constant = newHeight
        }
    }

    /// Changes the width constraint of the view. Makes sure that the constraint is set even if it wasn't previously assigned.
    /// - Parameter newHeight: The new width constraint to be set.
    func adjustWidth(_ newWidth: CGFloat) {
        if widthConstraint == nil {
            width(newWidth)
        } else {
            widthConstraint?.constant = newWidth
        }
    }
}
#endif
