//
//  UIStackView+ClearAllSubviews.swift
//  
//
//  Created by Lukáš Růžička on 08.01.2021.
//

#if canImport(UIKit)
import UIKit

public extension UIStackView {

    func clearAllSubviews() {
        subviews.forEach {
            $0.removeFromSuperview()
        }
    }
}
#endif
