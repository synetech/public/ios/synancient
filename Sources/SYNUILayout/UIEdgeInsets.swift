//
//  File.swift
//  
//
//  Created by Petr Dušek on 28/10/2019.
//

#if canImport(UIKit)
import UIKit

public extension UIEdgeInsets {

    static func makeInsets(_ inset: CGFloat) -> UIEdgeInsets {
        return UIEdgeInsets(top: inset, left: inset, bottom: inset, right: inset)
    }

    static func makeVerticalInsets(_ inset: CGFloat) -> UIEdgeInsets {
        return UIEdgeInsets(top: inset, left: 0, bottom: inset, right: 0)
    }

    static func makeHorizontalInsets(_ inset: CGFloat) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: inset, bottom: 0, right: inset)
    }
}
#endif
