//
//  UIView+AnchorToSafeArea.swift
//  
//
//  Created by Lukáš Růžička on 08/07/2020.
//

#if canImport(UIKit)
import Stevia
import UIKit

/// Following functions aligns the view to the safe area (not the window!)
/// Very useful for X iPhones, where is critical to not place views outside the top (notch) or the bottom (navigation line) of safe area
public extension UIView {

    // MARK: - Top
    /// Aligns the view to the top of the safe area of its superview.
    @discardableResult
    func safeAreaTop() -> Self {
        safeAreaTop(0)
    }

    /// Sets the top padding according to the safe area of its superview.
    @discardableResult
    func safeAreaTop(_ padding: CGFloat) -> Self {
        safeAreaTop(Double(padding))
    }

    /// Sets the top padding according to the safe area of its superview.
    @discardableResult
    func safeAreaTop(_ padding: Int) -> Self {
        safeAreaTop(Double(padding))
    }

    /// Sets the top padding according to the safe area of its superview.
    @discardableResult
    func safeAreaTop(_ padding: Double) -> Self {
        guard let superview = superview else {
            top(padding)
            return self
        }
        Top == superview.safeAreaLayoutGuide.Top + padding
        return self
    }

    // MARK: - Bottom
    /// Aligns the view to the bottom of the safe area of its superview.
    @discardableResult
    func safeAreaBottom() -> Self {
        safeAreaBottom(0)
    }

    /// Sets the bottom padding according to the safe area of its superview.
    @discardableResult
    func safeAreaBottom(_ padding: CGFloat) -> Self {
        safeAreaBottom(Double(padding))
    }

    /// Sets the bottom padding according to the safe area of its superview.
    @discardableResult
    func safeAreaBottom(_ padding: Int) -> Self {
        safeAreaBottom(Double(padding))
    }

    /// Sets the bottom padding according to the safe area of its superview.
    @discardableResult
    func safeAreaBottom(_ padding: Double) -> Self {
        guard let superview = superview else {
            bottom(padding)
            return self
        }
        Bottom == superview.safeAreaLayoutGuide.Bottom - padding
        return self
    }

    // MARK: - Left
    /// Aligns the view to the left of the safe area of its superview.
    @discardableResult
    func safeAreaLeft() -> Self {
        safeAreaLeft(0)
    }

    /// Sets the left padding according to the safe area of its superview.
    @discardableResult
    func safeAreaLeft(_ padding: CGFloat) -> Self {
        safeAreaLeft(Double(padding))
    }

    /// Sets the left padding according to the safe area of its superview.
    @discardableResult
    func safeAreaLeft(_ padding: Int) -> Self {
        safeAreaLeft(Double(padding))
    }

    /// Sets the left padding according to the safe area of its superview.
    @discardableResult
    func safeAreaLeft(_ padding: Double) -> Self {
        guard let superview = superview else {
            left(padding)
            return self
        }
        Left == superview.safeAreaLayoutGuide.Left + padding
        return self
    }

    // MARK: - Right
    /// Aligns the view to the right of the safe area of its superview.
    @discardableResult
    func safeAreaRight() -> Self {
        safeAreaRight(0)
    }

    /// Sets the right padding according to the safe area of its superview.
    @discardableResult
    func safeAreaRight(_ padding: CGFloat) -> Self {
        safeAreaRight(Double(padding))
    }

    /// Sets the right padding according to the safe area of its superview.
    @discardableResult
    func safeAreaRight(_ padding: Int) -> Self {
        safeAreaRight(Double(padding))
    }

    /// Sets the right padding according to the safe area of its superview.
    @discardableResult
    func safeAreaRight(_ padding: Double) -> Self {
        guard let superview = superview else {
            right(padding)
            return self
        }
        Right == superview.safeAreaLayoutGuide.Right - padding
        return self
    }
}
#endif
