//
//  UIView+FillSafeArea.swift
//  
//
//  Created by Lukáš Růžička on 09/11/2020.
//

#if canImport(UIKit)
import UIKit

public extension UIView {

    /// Aligns contraints on all sides to safe area
    /// - Parameter padding: Distance from safe area on all sides. Default is `0`.
    @discardableResult
    func fillSafeArea(_ padding: CGFloat = 0) -> UIView {
        safeAreaTop(padding).safeAreaBottom(padding).safeAreaLeft(padding).safeAreaRight(padding)
    }
}
#endif
