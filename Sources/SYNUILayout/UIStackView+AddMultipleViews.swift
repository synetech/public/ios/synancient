//
//  UIStackView+AddMultipleViews.swift
//  
//
//  Created by Lukáš Růžička on 09/11/2020.
//

#if canImport(UIKit)
import UIKit

public extension UIStackView {

    func addArrangedSubviews(_ views: [UIView]) {
        views.forEach { addArrangedSubview($0) }
    }
}
#endif
