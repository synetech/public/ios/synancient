//
//  UIView+OnTap.swift
//  
//
//  Created by Tom Novotny on 19.11.2021.
//

#if canImport(UIKit)
import UIKit

public extension UIView {

    /// Handles tap gesture by wrapping the passed function into an underlying Selector
    @discardableResult
    func onTap(_ function: @escaping () -> Void) -> Self {
        onTapGesture(function)
    }

    @discardableResult
    func addOnTap(_ function: @escaping () -> Void) -> Self {
        onTapGesture(function)
    }

    @discardableResult
    func onTapGesture(_ function: @escaping () -> Void) -> Self {
        addGestureRecognizer(
            TapGestureRecognizer { function() }
        )
        return self
    }
}

private class TapGestureRecognizer: UITapGestureRecognizer {

    public let perform: Selector.Perform

    init(_ perform: @escaping () -> Void) {
        self.perform = .init(perform)
        super.init(target: self.perform, action: self.perform.selector)
    }
}

private extension Selector {

    class Perform {
        private let perform: () -> Void

        init(_ perform: @escaping () -> Void) {
            self.perform = perform
        }
    }
}

private extension Selector.Perform {

    var selector: Selector { #selector(call) }

    @objc
    func call() { perform() }

}
#endif
