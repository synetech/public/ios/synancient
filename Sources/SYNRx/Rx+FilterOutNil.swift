//
//  Rx+FilterOutNil.swift
//  
//
//  Created by Lukáš Růžička on 08.01.2021.
//

import RxCocoa
import RxSwift

public extension ObservableType {
    
    /// Filters out nil values and cast the `Element` type to non-optional.
    /// - Warning: Be sure that the chain emit `non-nil` value at some point because otherwise, it will remain stuck and doesn't emit anything ever.
    /// - Returns: Observable with non-optional `Element` type.
    func filterOutNil<T>() -> Observable<T> where Element == T? {
        return compactMap { $0 }
    }
}

public extension Driver where SharingStrategy == DriverSharingStrategy {

    /// Filters out nil values and cast the `Element` type to non-optional.
    /// - Warning: Be sure that the chain emit `non-nil` value at some point because otherwise, it will remain stuck and doesn't emit anything ever.
    /// - Returns: Driver with non-optional `Element` type.
    func filterOutNil<T>() -> Driver<T> where Element == T? {
        return compactMap { $0 }
    }
}
