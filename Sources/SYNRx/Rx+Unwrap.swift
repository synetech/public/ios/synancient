//
//  Rx+Unwrap.swift
//  
//
//  Created by Lukáš Růžička on 08.01.2021.
//

import RxSwift

public extension ObservableType {
    
    /// Unwraps all `nil` values and emits given error instead.
    /// - Parameter error: Error to be thrown when `nil` value is emitted.
    /// - Returns: Observable with non-optional `Element` type.
    func unwrap<T>(onNilThrow error: Error) -> Observable<T> where Element == T? {
        return map {
            guard let value = $0 else { throw error }
            return value
        }
    }

    /// Unwraps all `nil` values and emits given value instead.
    /// - Parameter element: Value to be returned when `nil` value is emitted.
    /// - Returns: Observable with non-optional `Element` type.
    func unwrap<T>(onNilJustReturn element: T) -> Observable<T> where Element == T? {
        return map {
            guard let value = $0 else { return element }
            return value
        }
    }
}

