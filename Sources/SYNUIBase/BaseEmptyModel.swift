//
//  BaseEmptyModel.swift
//  
//
//  Created by Štěpán Klouček on 14/08/2020.
//

import Foundation

///Needs to be struct othervise will not function correctly
///with BaseViewModel
public protocol BaseEmptyModel {
    static func createEmpty() -> Self
}
