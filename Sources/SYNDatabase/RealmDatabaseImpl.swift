//
//  File.swift
//  
//
//  Created by Vojtěch Pajer on 08/11/2019.
//

import Foundation
import RealmSwift
import RxSwift

public class RealmDatabaseImpl: Database {
    
    //MARK: Enum
    public enum DatabaseError: Error {
        // TODO: Extend
        case unsupportedIdType
        case objectNotFound
        case objectWasInvalidated
    }
    
    public typealias MigrationBlock = RealmSwift.MigrationBlock?
    typealias Configuration = Realm.Configuration
    
    typealias Operation = (Realm) -> Void
    typealias ErrorCallback = (Error) -> Void
    typealias NoParameterCallback = () -> Void
    
    //MARK: - Properties
    private let databaseSchemaVersion: UInt64
    private let migrationBlock: MigrationBlock
    private let inMemoryIdentifier: String?

    private var config: Configuration {
        var config = Configuration()

        config.schemaVersion = databaseSchemaVersion
        config.migrationBlock = migrationBlock
        config.deleteRealmIfMigrationNeeded = migrationBlock == nil
        if let inMemoryIdentifier = inMemoryIdentifier {
            config.inMemoryIdentifier = inMemoryIdentifier
        }

        #if DEBUG
        if let fileLocation = config.fileURL?.absoluteString {
            print("[DEBUG] Realm location is " + fileLocation)
        } else {
            print("File not found")
        }
        #endif
        return config
    }

    // MARK: - Init
    public init(databaseSchemaVersion: UInt64, migrationBlock: MigrationBlock = nil, inMemoryIdentifier: String? = nil) {
        self.databaseSchemaVersion = databaseSchemaVersion
        self.migrationBlock = migrationBlock
        self.inMemoryIdentifier = inMemoryIdentifier
    }
}

// MARK: - Add
public extension RealmDatabaseImpl {
    
    func add<T: Object>(_ object: T) -> Completable {
        return simpleOperation(operation: { realm in realm.add(object, update: .modified) })
    }
    
    func addAll<T: Object>(_ objects: [T]) -> Completable {
        return simpleOperation(operation: { realm in realm.add(objects, update: .modified) })
    }
}

// MARK: - Update
public extension RealmDatabaseImpl {
    
    func update<T: Object>(_ object: T, updateBlock: @escaping (T) -> Void) -> Completable {
        return simpleOperation(operation: { _ in updateBlock(object) })
    }
    
    func update<T: Object, V>(_ object: T, changedProperty: ReferenceWritableKeyPath<T, V>, value: V) -> Completable {
        return simpleOperation(operation: { _ in object[keyPath: changedProperty] = value })
    }
    
    func updateAll<T: Object>(_ objects: [T], updateBlock: @escaping (T) -> Void) -> Completable {
        return simpleOperation(operation: { _ in objects.forEach(updateBlock) })
    }
    
    func updateAll<T: Object, V>(_ objects: [T],
                                 changedProperty: ReferenceWritableKeyPath<T, V>,
                                 value: V) -> Completable {
        return simpleOperation(operation: { _ in objects.forEach { $0[keyPath: changedProperty] = value } })
    }
    
    func updateAll<T: Object>(_ type: T.Type, updateBlock: @escaping (T) -> Void) -> Completable {
        return simpleOperation(operation: { realm in realm.objects(type).forEach(updateBlock) })
    }
    
    func updateAll<T: Object, V>(_ type: T.Type,
                                 changedProperty: ReferenceWritableKeyPath<T, V>,
                                 value: V) -> Completable {
        return simpleOperation(operation: { realm in
            realm.objects(type).forEach { $0[keyPath: changedProperty] = value }
        })
    }
    
    func update<T: Object, U>(_ type: T.Type, id: U, updateBlock: @escaping (T) -> Void) -> Completable {
        return !isIdValid(id) ?
            Completable.error(DatabaseError.unsupportedIdType) :
            Completable.create(subscribe: { [weak self] completable in
                self?.writeOperation(operation: { realm in
                    if let object = realm.object(ofType: type, forPrimaryKey: id) {
                        updateBlock(object)
                        completable(.completed)
                    } else {
                        completable(.error(DatabaseError.objectNotFound))
                    }
                }, onError: { error in
                    completable(.error(error))
                })
                return Disposables.create()
            })
    }
    
    func update<T: Object, U, V>(_ type: T.Type, id: U,
                                 changedProperty: ReferenceWritableKeyPath<T, V>,
                                 value: V) -> Completable {
        return update(type, id: id, updateBlock: { $0[keyPath: changedProperty] = value })
    }
}

// MARK: - Update with predicate
public extension RealmDatabaseImpl {
    
    func updateAllWithPredicate<T: Object>(_ type: T.Type, predicate: NSPredicate,
                                           updateBlock: @escaping (T) -> Void) -> Completable {
        return simpleOperation(operation: { realm in
            realm.objects(type).filter(predicate).forEach(updateBlock)
        })
    }
    
    func updateAllWithPredicate<T: Object, V>(_ type: T.Type, predicate: NSPredicate,
                                              changedProperty: ReferenceWritableKeyPath<T, V>,
                                              value: V) -> Completable {
        return simpleOperation(operation: { realm in
            realm.objects(type).filter(predicate).forEach { $0[keyPath: changedProperty] = value }
        })
    }
}


// MARK: - Get
public extension RealmDatabaseImpl {
    
    func get<T: Object, U>(_ type: T.Type, id: U) -> Observable<T> {
        return !isIdValid(id) ?
            Observable.error(DatabaseError.unsupportedIdType) :
            getOperation(getObjectOperation: { realm in realm.object(ofType: T.self, forPrimaryKey: id) })
    }
    
    func getOnce<T: Object, U>(_ type: T.Type, id: U) -> Single<T> {
        return !isIdValid(id) ?
            Single.error(DatabaseError.unsupportedIdType) :
            getOperation(getObjectOperation: { realm in realm.object(ofType: type, forPrimaryKey: id) })
    }
    
    func getFirst<T: Object>(_ type: T.Type) -> Observable<T> {
        return getOperation(getObjectOperation: { realm in realm.objects(type).first })
    }
    
    func getFirstOnce<T: Object>(_ type: T.Type) -> Single<T> {
        return getOperation(getObjectOperation: { realm in realm.objects(type).first })
    }
    
    func getAll<T: Object>(_ type: T.Type) -> Observable<Results<T>> {
        return getAllOperation(getObjectsOperation: { realm in realm.objects(T.self) })
    }
    
    
    func getAllOnce<T: Object>(_ type: T.Type) -> Single<Results<T>> {
        return getAllOperation(getObjectsOperation: { realm in realm.objects(type) })
    }
}

// MARK: - Get with predicate
public extension RealmDatabaseImpl {
    
    func getFirst<T: Object>(_ type: T.Type,
                             predicate: NSPredicate) -> Observable<T> {
        return getOperation(getObjectOperation: { realm in realm.objects(type).filter(predicate).first })
    }
    
    func getFirstOnce<T: Object>(_ type: T.Type,
                                 predicate: NSPredicate) -> Single<T> {
        return getOperation(getObjectOperation: { realm in realm.objects(type).filter(predicate).first })
    }
    
    func getWithPredicate<T: Object>(_ type: T.Type,
                                     predicate: NSPredicate) -> Observable<Results<T>> {
        return getAllOperation(getObjectsOperation: { realm in realm.objects(type).filter(predicate) })
    }
    
    func getWithPredicate<T: Object>(_ type: T.Type,
                                     predicate: Observable<NSPredicate>) -> Observable<Results<T>> {
        return Observable.create({ [unowned self] observable in
            var disposable: Disposable?
            let observable = predicate.subscribe(onNext: { newPredicate in
                disposable?.dispose()
                disposable = self.getWithPredicate(type, predicate: newPredicate)
                    .subscribe(onNext: observable.onNext,
                               onError: observable.onError,
                               onCompleted: observable.onCompleted)
            },
                                onError: observable.onError,
                                onCompleted: observable.onCompleted)
            return Disposables.create {
                observable.dispose()
            }
        })
    }
    
    func getOnceWithPredicate<T: Object>(_ type: T.Type,
                                         predicate: NSPredicate) -> Single<Results<T>> {
        return getAllOperation(getObjectsOperation: { realm in realm.objects(type).filter(predicate) })
    }
}

// MARK: - Delete
public extension RealmDatabaseImpl {
    
    func delete<T: Object>(_ object: T) -> Completable {
        return simpleOperation(operation: { realm in realm.delete(object) })
    }
    
    func delete<T: Object, U>(_ type: T.Type, id: U) -> Completable {
        return !isIdValid(id) ?
            Completable.error(DatabaseError.unsupportedIdType) :
            Completable.create(subscribe: { [weak self] completable in
                self?.writeOperation(operation: { realm in
                    if let object = realm.object(ofType: T.self, forPrimaryKey: id) {
                        realm.delete(object)
                        completable(.completed)
                    } else {
                        completable(.error(DatabaseError.objectNotFound))
                    }
                }, onError: { error in
                    completable(.error(error))
                })
                return Disposables.create()
            })
    }
    
    func deleteAll<T: Object>(_ type: T.Type) -> Completable {
        return simpleOperation(operation: { realm in
            let objects = realm.objects(type)
            realm.delete(objects)
        })
    }
    
    func deleteAll<T: Object>(_ objects: [T]) -> Completable {
        return simpleOperation(operation: { realm in realm.delete(objects) })
    }
    
    func deleteWithPredicate<T: Object>(_ type: T.Type, predicate: NSPredicate) -> Completable {
        return simpleOperation(operation: { realm in
            let objects = realm.objects(type).filter(predicate)
            realm.delete(objects)
        })
    }
    
    func deleteCurrentAndAddAll<T: Object>(_ objects: [T]) -> Completable {
        return simpleOperation(operation: { realm in
            let objectsToDelete = realm.objects(T.self)
            realm.delete(objectsToDelete)
            realm.add(objects, update: .modified)
        })
    }
    
    func deleteDatabase() {
        let realm = try? Realm(configuration: config)
        try? realm?.write {
          realm?.deleteAll()
        }
    }
}

// MARK: - Operations
private extension RealmDatabaseImpl {
    
    func simpleOperation(operation: @escaping Operation) -> Completable {
        return Completable.create(subscribe: { [weak self] completable in
            self?.writeOperation(operation: { realm in
                operation(realm)
                completable(.completed)
            }, onError: { error in
                completable(.error(error))
            })
            return Disposables.create()
        })
    }
    
    func getOperation<T: Object>(getObjectOperation: @escaping (Realm) -> T?) -> Observable<T> {
        return Observable.create({ [weak self] observable in
            var token: NotificationToken?
            self?.readOperation(operation: { realm in
                if let object = getObjectOperation(realm) {
                    observable.onNext(object)
                    token = object.observe({ (_) in
                        object.isInvalidated ?
                            observable.onError(DatabaseError.objectWasInvalidated) :
                            observable.onNext(object)
                    })
                } else {
                    observable.onError((DatabaseError.objectNotFound))
                }
            }, onError: { error in
                observable.onError(error)
            })
            return Disposables.create {
                token?.invalidate()
            }
        })
    }
    
    func getOperation<T: Object>(getObjectOperation: @escaping (Realm) -> T?) -> Single<T> {
        return Single.create(subscribe: { [weak self] trait in
            self?.readOperation(operation: { realm in
                if let object = getObjectOperation(realm) {
                    trait(.success(object))
                } else {
                    trait(.failure(DatabaseError.objectNotFound))
                }
            }, onError: { error in
                trait(.failure(error))
            })
            return Disposables.create()
        })
    }
    
    func getAllOperation<T: Object>(getObjectsOperation: @escaping (Realm) -> Results<T>) -> Observable<Results<T>> {
        return Observable.create({ [weak self] observable in
            var token: NotificationToken?
            self?.readOperation(operation: { realm in
                let objects = getObjectsOperation(realm)
                observable.onNext(objects)
                token = objects.observe { changeset in
                    switch changeset {
                    case.initial:
                        break
                    case .update(let values, _, _, _):
                        observable.onNext(values)
                    case.error(let error):
                        observable.onError(error)
                    }
                }
            }, onError: { error in
                observable.onError(error)
            })
            return Disposables.create {
                token?.invalidate()
            }
        })
    }
    
    func getAllOperation<T: Object>(getObjectsOperation: @escaping (Realm) -> Results<T>) -> Single<Results<T>> {
        return Single.create(subscribe: { [weak self] trait in
            self?.readOperation(operation: { realm in
                let objects = getObjectsOperation(realm)
                trait(.success(objects))
            }, onError: { error in
                trait(.failure(error))
            })
            return Disposables.create()
        })
    }
    
    func writeOperation(operation: @escaping Operation,
                        onError: ErrorCallback,
                        onCompleted: NoParameterCallback? = nil) {
        do {
            let realm = try Realm(configuration: config)
            
            try write(operation: operation, to: realm)
            onCompleted?()
        } catch {
            handleError(error, errorCallback: onError)
        }
    }
    
    func readOperation(operation: Operation, onError: ErrorCallback) {
        do {
            let realm = try Realm(configuration: config)
            operation(realm)
        } catch {
            handleError(error, errorCallback: onError)
        }
    }
}

// MARK: - Private
private extension RealmDatabaseImpl {
    
    func write(operation: Operation, to realm: Realm) throws {
        if realm.isInWriteTransaction {
            operation(realm)
            try realm.commitWrite()
        } else {
            try realm.write {
                operation(realm)
                try realm.commitWrite()
            }
        }
    }
    
    func handleError(_ error: Error, errorCallback: ErrorCallback) {
        print("FATAL: Error while interacting with Realm!\n\(error)")
        errorCallback(error)
    }
    
    func isIdValid<U>(_ id: U) -> Bool {
        return id is String || id is Int || id is Bool || id is NSDate
    }
}
