//
//  File.swift
//  
//
//  Created by Vojtěch Pajer on 08/11/2019.
//

import Foundation
import RealmSwift
import RxSwift

public protocol Database {
    
    // MARK: - Add
    func add<T: Object>(_ object: T) -> Completable
    func addAll<T: Object>(_ objects: [T]) -> Completable
    
    // MARK: - Update
    func update<T: Object>(_ object: T, updateBlock: @escaping (T) -> Void) -> Completable
    func update<T: Object, V>(_ object: T,
                              changedProperty: ReferenceWritableKeyPath<T, V>,
                              value: V) -> Completable
    func update<T: Object, U>(_ type: T.Type, id: U, updateBlock: @escaping (T) -> Void) -> Completable
    func update<T: Object, U, V>(_ type: T.Type, id: U,
                                 changedProperty: ReferenceWritableKeyPath<T, V>,
                                 value: V) -> Completable
    func updateAll<T: Object>(_ objects: [T], updateBlock: @escaping (T) -> Void) -> Completable
    func updateAll<T: Object, V>(_ objects: [T],
                                 changedProperty: ReferenceWritableKeyPath<T, V>,
                                 value: V) -> Completable
    func updateAll<T: Object>(_ type: T.Type, updateBlock: @escaping (T) -> Void) -> Completable
    func updateAll<T: Object, V>(_ type: T.Type,
                                 changedProperty: ReferenceWritableKeyPath<T, V>,
                                 value: V) -> Completable
    
    // MARK: - Update with predicate
    func updateAllWithPredicate<T: Object>(_ type: T.Type, predicate: NSPredicate,
                                           updateBlock: @escaping (T) -> Void) -> Completable
    func updateAllWithPredicate<T: Object, V>(_ type: T.Type, predicate: NSPredicate,
                                              changedProperty: ReferenceWritableKeyPath<T, V>,
                                              value: V) -> Completable
    
    // MARK: - Get
    func get<T: Object, U>(_ type: T.Type, id: U) -> Observable<T>
    func getOnce<T: Object, U>(_ type: T.Type, id: U) -> Single<T>
    func getFirst<T: Object>(_ type: T.Type) -> Observable<T>
    func getFirstOnce<T: Object>(_ type: T.Type) -> Single<T>
    func getAll<T: Object>(_ type: T.Type) -> Observable<Results<T>>
    func getAllOnce<T: Object>(_ type: T.Type) -> Single<Results<T>>
    
    // MARK: - Get with predicate
    func getFirst<T: Object>(_ type: T.Type, predicate: NSPredicate) -> Observable<T>
    func getFirstOnce<T: Object>(_ type: T.Type, predicate: NSPredicate) -> Single<T>
    func getWithPredicate<T: Object>(_ type: T.Type, predicate: NSPredicate) -> Observable<Results<T>>
    func getWithPredicate<T: Object>(_ type: T.Type, predicate: Observable<NSPredicate>) -> Observable<Results<T>>
    func getOnceWithPredicate<T: Object>(_ type: T.Type, predicate: NSPredicate) -> Single<Results<T>>
    
    // MARK: - Delete
    func delete<T: Object>(_ object: T) -> Completable
    func delete<T: Object, U>(_ type: T.Type, id: U) -> Completable
    func deleteAll<T: Object>(_ type: T.Type) -> Completable
    func deleteAll<T: Object>(_ objects: [T]) -> Completable
    func deleteWithPredicate<T: Object>(_ type: T.Type, predicate: NSPredicate) -> Completable
    func deleteCurrentAndAddAll<T: Object>(_ objects: [T]) -> Completable
    
    func deleteDatabase()
}
