# SYNAncient

All the things from other SYNLibraries that are dependant on frameworks we no longer use, such as RxSwift or Stevia.

## UI

### Base
Base classes can be used with lasagne architecture, to remove some of common handling and boiler plate from parent class.

### Layout

Set of `UIKit` components extensions. 
Extensions for `UIView`, `UIEdgeInsets` and `UIViewController`. 

_Contains:_

1. `UIView`
    - `isShown`, `roundCorners`
    - adjusting height/width regardless of whether the constraint was assigned before
2. `UIEdgeInsets`
3. `Safe area anchoring` 
    - extension on Stevia for anchoring to the safe area of superview
    - can be used like Stevia's `top`/`bottom` etc. -> `safeAreaTop`/`safeAreaBottom` etc.
    - you can also fill whole safe area with `fillSafeArea`
4. `UIStackView`
    - adding multiple arranged subviews at once
    - clearing all subviews
5. `UIImageView`
    - adjusting width/height based on already assigned height/width and the image ratio 
6. `UITableView`
    - dynamic changes of table footer height

### Keyboard

**Keyboard offset handler**
Provides handler for moving text fields and other views when keyboard appears to remain visible. 
All public methods is documented within the code, so briefly:
- All the text fields needs to be wrapped in container, which then will be moved (so be careful with which all views it contains).
- You need to initialize the `KeyboardOffsetHandler` and store it as class property of your `UIViewController`. Pass the view container to the init.
- Call `ensureVisibility` method on the instance and pass all the text fields and other views which should remain visible, e.g. login button (keep in mind, that the other view can't be wrapped in the container above!).
- You can also set height of the space which should remain under the active text field to make sure that the other text field is visible. The last text field doesn't create the space under by default, but you can explicitly allow it, if it makes sense in your case (e.g. you have hint under the field).

All passed views should then react to the keyboard height and move appropriately 😎.



## Utils

### Rx
Useful extensions for `RxSwift`/`RxCocoa`.

#### FilterOutNil
Chain function which filters nil values and maps them to non-optional value. Available for `Observable` and `Driver`.

#### Unwrap
Chain function which unwraps optional value to non-optional. Unlike `FilterOutNil` it doesn't ignore emitted `nil` values and returns default value or throws error instead.



## Data sources

### Database Storage
`Database` protocol and `RealmDatabaseImpl` implementation is provided. You only need to register it in your container and it's ready to use. Just extend your DB models with `RealmObject` from `DatabaseObjects` target.
