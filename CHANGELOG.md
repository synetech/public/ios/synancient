## 1.0.0 Release notes (11-11-2022)

### Breaking Changes
- Package init
- Added ancient things from SYNUI
- Added ancient things from SYNDataSources
- Added ancient things from SYNUtils


## x.x.x Release notes (dd-mm-yyyy)

### Breaking Changes
### Deprecated
### Enhancements
### Bugfixes
