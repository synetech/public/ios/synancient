// swift-tools-version: 5.7
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

enum PackageProduct: String, CaseIterable {
    
    case wholePackage = "SYNAncient"

    var name: String {
        return rawValue
    }

    var product: Product {
        return .library(name: rawValue, targets: targets)
    }

    var targets: [String] {
        switch self {
        case .wholePackage:
            return PackageTarget.allCases.map { $0.name }
        }
    }
}

enum PackageDependency: String, CaseIterable {

    case stevia = "Stevia"
    case rxSwift = "RxSwift"
    case rxCocoa = "RxCocoa"
    case realm = "RealmSwift"

    var dependency: Target.Dependency {
        switch self {
        case .rxCocoa, .rxSwift:
            return .product(name: rawValue, package: "RxSwift")
        case .realm:
            return .product(name: rawValue, package: "realm-cocoa")
        default:
            return Target.Dependency(stringLiteral: rawValue)
        }
    }

    static var packages: [Package.Dependency] {
        return [
            .package(url: "https://github.com/freshOS/Stevia", from: "5.1.2"),
            .package(url: "https://github.com/ReactiveX/RxSwift", from: "6.0.0"),
            .package(url: "https://github.com/realm/realm-cocoa.git", from: "5.5.2")
        ]
    }
}

enum PackageTarget: String, CaseIterable {

    case uiBase = "SYNUIBase"
    case uiLayout = "SYNUILayout"
    case uiKeyboard = "SYNUIKitKeyboard"
    case uRX = "SYNRx"
    case dsDatabase = "SYNDatabase"
    case dsDatabaseObjects = "SYNDatabaseObjects"

    var name: String {
        return rawValue
    }

    var dependency: Target.Dependency {
        return Target.Dependency(stringLiteral: rawValue)
    }

    var dependencies: [Target.Dependency] {
        switch self {
        case .uiBase:
            return [PackageDependency.rxCocoa.dependency, PackageDependency.rxSwift.dependency]
        case .uiLayout:
            return [PackageDependency.stevia.dependency]
        case .uiKeyboard:
            return [PackageDependency.rxSwift.dependency, PackageDependency.rxCocoa.dependency]
        case .uRX:
            return [PackageDependency.rxSwift.dependency, PackageDependency.rxCocoa.dependency]
        case .dsDatabase:
            return [PackageDependency.realm.dependency, PackageDependency.rxSwift.dependency]
        case .dsDatabaseObjects:
            return [PackageDependency.realm.dependency]
        }
    }

    static var targets: [Target] {
        return allCases.map { Target.target(name: $0.name, dependencies: $0.dependencies) }
    }
}

enum PackageTestTarget: String, CaseIterable {

    case empty = "EmptyTests"

    var name: String {
        return rawValue
    }

    var dependencies: [Target.Dependency] {
        switch self {
        case .empty:
            return []
        }
    }

    static var targets: [Target] {
        return allCases.map { Target.testTarget(name: $0.name, dependencies: $0.dependencies) }
    }
}

let package = Package(
    name: "SYNAncient",
    platforms: [.iOS(.v11), .macOS(.v10_14)],
    // Products define the executables and libraries produced by a package, and make them visible to other packages.
    products: PackageProduct.allCases.map { $0.product },
    // Dependencies declare other packages that this package depends on.
    dependencies: PackageDependency.packages,
    // Targets are the basic building blocks of a package. A target can define a module or a test suite.
    // Targets can depend on other targets in this package, and on products in packages which this package depends on.
    targets: PackageTarget.targets + PackageTestTarget.targets
)
