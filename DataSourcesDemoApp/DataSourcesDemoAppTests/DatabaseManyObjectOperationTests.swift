//
//  DatabaseManyObjectOperationTests.swift
//  DatabaseManyObjectOperationTests
//
//  Created by Vojtěch Pajer on 13/11/2019.
//  Copyright © 2019 Vojtěch Pajer. All rights reserved.
//

import SYNDatabase
import RxBlocking
import RxSwift
import XCTest

@testable import DataSourcesDemoApp

final class DatabaseManyObjectOperationTests: XCTestCase {
    
    var databaseInstance: Database {
        return RealmDatabaseImpl(databaseSchemaVersion: 1, migrationBlock: nil, inMemoryIdentifier: UUID().uuidString)
    }
    let disposeBag = DisposeBag()
    
    func testGetAll_Simple() {
        let database = databaseInstance
        let person1 = PersonDBEntity()
        let person2 = PersonDBEntity()
        let people = [person1, person2]

        _ = try! database.addAll(people).toBlocking().first()
        
        let addedPeople = try! database.getAllOnce(PersonDBEntity.self)
            .toBlocking()
            .first()
        
        XCTAssertEqual(addedPeople?.count, people.count)
        XCTAssert(addedPeople?.contains(person1) ?? false)
        XCTAssert(addedPeople?.contains(person2) ?? false)
    }
    
    func testGetAll_AddToEmpty() {
        let database = databaseInstance
        let person = PersonDBEntity()
        let expectation = XCTestExpectation(description: "Event was triggered")

        database.getAll(PersonDBEntity.self)
            .subscribe(onNext: { people in
                if !people.isEmpty { expectation.fulfill() }
            })
            .disposed(by: disposeBag)

        _ = try! database.add(person).toBlocking().first()

        wait(for: [expectation], timeout: 2.0)
    }
    
    func testGetAll_RemoveAllAndAdd() {
        let database = databaseInstance
        let person1 = PersonDBEntity()
        let expectation = XCTestExpectation(description: "Event was triggered")
        expectation.expectedFulfillmentCount = 2
        
        _ = try! database.add(person1).toBlocking().first()
        database.getAll(PersonDBEntity.self)
            .subscribe(onNext: { people in
                if !people.isEmpty { expectation.fulfill() }
            })
            .disposed(by: disposeBag)
        _ = try! database.delete(person1).toBlocking().first()
        
        let person2 = PersonDBEntity()
        _ = try! database.add(person2).toBlocking().first()
        
        wait(for: [expectation], timeout: 2.0)
    }
    
    func testUpdateAllObjects() {
        let database = databaseInstance
        let queriedAge = 40
        let newCityValue = "Omsk"
        let newNameValue = "Gregor"
        let people = [
            PersonDBEntity(name: "Jindra", age: queriedAge), PersonDBEntity(name: "Hector", age: 82),
            PersonDBEntity(name: "Marcel", age: queriedAge), PersonDBEntity(name: "Jirka", age: 12),
        ]
        let peopleToUpdate = people.filter { $0.db_age == queriedAge }
        
        _ = try! database.addAll(people).toBlocking().first()
        _ = try! database.updateAll(peopleToUpdate, updateBlock: { person in
            person.db_city = newCityValue
        }).toBlocking().first()
        _ = try! database.updateAll(peopleToUpdate,
                                    changedProperty: \PersonDBEntity.db_fullName,
                                    value: newNameValue)
            .toBlocking().first()
        
        let peopleAfterUpdate = try! database.getAll(PersonDBEntity.self).toBlocking().first()
        let peopleWithNewNameCount = peopleAfterUpdate?.filter { $0.db_city == newCityValue }.count
        let peopleWithNewCityCount = peopleAfterUpdate?.filter { $0.db_fullName == newNameValue }.count
        
        XCTAssertEqual(peopleWithNewNameCount, peopleToUpdate.count)
        XCTAssertEqual(peopleWithNewCityCount, peopleToUpdate.count)
    }
    
    func testUpdateAll() {
        let database = databaseInstance
        let newCityValue = "Omsk"
        let newNameValue = "Gregor"
        let people = [
            PersonDBEntity(name: "Jindra", age: 54), PersonDBEntity(name: "Hector", age: 82),
            PersonDBEntity(name: "Marcel", age: 11), PersonDBEntity(name: "Jirka", age: 12),
        ]
        
        _ = try! database.addAll(people).toBlocking().first()
        _ = try! database.updateAll(PersonDBEntity.self, updateBlock: { person in
            person.db_city = newCityValue
        }).toBlocking().first()
        _ = try! database.updateAll(PersonDBEntity.self,
                                    changedProperty: \PersonDBEntity.db_fullName,
                                    value: newNameValue)
            .toBlocking().first()
        
        let peopleAfterUpdate = try! database.getAll(PersonDBEntity.self).toBlocking().first()
        let peopleWithNewNameCount = peopleAfterUpdate?.filter { $0.db_city == newCityValue }.count
        let peopleWithNewCityCount = peopleAfterUpdate?.filter { $0.db_fullName == newNameValue }.count
        
        XCTAssertEqual(peopleWithNewNameCount, people.count)
        XCTAssertEqual(peopleWithNewCityCount, people.count)
    }
    
    func testDeleteAllObjects() {
        let database = databaseInstance
        let queriedAge = 40
        let people = [
            PersonDBEntity(name: "Jindra", age: queriedAge), PersonDBEntity(name: "Hector", age: 82),
            PersonDBEntity(name: "Marcel", age: queriedAge), PersonDBEntity(name: "Jirka", age: 12),
        ]
        let peopleToDelete = people.filter { $0.db_age == queriedAge }
        let remainingPeopleCount = people.filter { $0.db_age != queriedAge }.count
        
        _ = try! database.addAll(people).toBlocking().first()
        _ = try! database.deleteAll(peopleToDelete).toBlocking().first()
        let remainingPeople = try! database.getAll(PersonDBEntity.self).toBlocking().first()
        
        XCTAssertEqual(remainingPeopleCount, remainingPeople?.count)
    }
    
    func testDeleteAll() {
        let database = databaseInstance
        let people = [
            PersonDBEntity(name: "Jindra", age: 40), PersonDBEntity(name: "Hector", age: 82),
            PersonDBEntity(name: "Marcel", age: 2), PersonDBEntity(name: "Jirka", age: 12),
        ]
        
        _ = try! database.addAll(people).toBlocking().first()
        _ = try! database.deleteAll(PersonDBEntity.self).toBlocking().first()
        let remainingPeople = try! database.getAll(PersonDBEntity.self).toBlocking().first()
        
        XCTAssert(remainingPeople?.isEmpty ?? false)
    }
    
    func testDeleteCurrentAndAddAll() {
        let database = databaseInstance
        let initialPeople = [
            PersonDBEntity(name: "Jindra", age: 40), PersonDBEntity(name: "Hector", age: 82),
            PersonDBEntity(name: "Marcel", age: 2), PersonDBEntity(name: "Jirka", age: 12),
        ]
        let newPeople = [
            PersonDBEntity(name: "Marie", age: 2), PersonDBEntity(name: "Beata", age: 12),
        ]
        
        _ = try! database.addAll(initialPeople).toBlocking().first()
        _ = try! database.deleteCurrentAndAddAll(newPeople).toBlocking().first()
        let people = try! database.getAll(PersonDBEntity.self).toBlocking().first()
        
        XCTAssertEqual(people?.count, newPeople.count)
    }
}
