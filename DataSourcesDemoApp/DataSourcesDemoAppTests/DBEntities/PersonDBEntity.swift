//
//  PersonDBEntity.swift
//  DataSourcesDemoApp
//
//  Created by Vojtěch Pajer on 13/11/2019.
//  Copyright © 2019 Vojtěch Pajer. All rights reserved.
//

import Foundation
import RealmSwift

class PersonDBEntity: Object {
    
    @objc dynamic var db_id: String = UUID().uuidString
    @objc dynamic var db_fullName: String = "Tomas Valenta"
    @objc dynamic var db_age: Int = 29
    @objc dynamic var db_birthday: Date = Date(timeIntervalSince1970: 1234)
    @objc dynamic var db_country: String = "Italy"
    @objc dynamic var db_city: String = "Oslo"
    @objc dynamic var db_phoneNumber: String = "123412666"
    @objc dynamic var db_sex: String = "M"
    @objc dynamic var db_: String = "Yes"
    
    
    override static func primaryKey() -> String? {
        return "db_id"
    }
    
    convenience init(name: String, age: Int) {
        self.init()
        
        db_fullName = name
        db_age = age
    }
}
