//
//  DatabaseErrorTests.swift
//  DataSourcesDemoAppTests
//
//  Created by Vojtěch Pajer on 14/11/2019.
//  Copyright © 2019 Vojtěch Pajer. All rights reserved.
//

import XCTest
import RxBlocking
import SYNDatabase

@testable import DataSourcesDemoApp

final class DatabaseErrorTests: XCTestCase {
    
    var databaseInstance: Database {
        return RealmDatabaseImpl(databaseSchemaVersion: 1, migrationBlock: nil, inMemoryIdentifier: UUID().uuidString)
    }
    
    func testError_objectNotFound() {
        let database = databaseInstance
        let customId = "ASD1323HRTH"
        
        do {
            _ = try database.get(PersonDBEntity.self, id: customId).toBlocking().first()
            XCTFail("No error emitted")
        } catch let error {
            guard case RealmDatabaseImpl.DatabaseError.objectNotFound = error else {
                XCTFail("Invalid error type")
                return
            }
        }
        
        do {
            _ = try database.update(PersonDBEntity.self, id: customId, updateBlock: { _ in }).toBlocking().first()
            XCTFail("No error emitted")
        } catch let error {
            guard case RealmDatabaseImpl.DatabaseError.objectNotFound = error else {
                XCTFail("Invalid error type")
                return
            }
        }
        
        do {
            _ = try database.delete(PersonDBEntity.self, id: customId).toBlocking().first()
            XCTFail("No error emitted")
        } catch let error {
            guard case RealmDatabaseImpl.DatabaseError.objectNotFound = error else {
                XCTFail("Invalid error type")
                return
            }
        }
    }
    
    func testError_unsupportedIdType() {
        let database = databaseInstance
        let invalidId = CGRect()
        
        do {
            _ = try database.get(PersonDBEntity.self, id: invalidId).toBlocking().first()
            XCTFail("No error emitted")
        } catch let error {
            guard case RealmDatabaseImpl.DatabaseError.unsupportedIdType = error else {
                XCTFail("Invalid error type")
                return
            }
        }
        do {
            _ = try database.delete(PersonDBEntity.self, id: invalidId).toBlocking().first()
            XCTFail("No error emitted")
        } catch let error {
            guard case RealmDatabaseImpl.DatabaseError.unsupportedIdType = error else {
                XCTFail("Invalid error type")
                return
            }
        }
        do {
            _ = try database.update(PersonDBEntity.self, id: invalidId, updateBlock: { _ in }).toBlocking().first()
            XCTFail("No error emitted")
        } catch let error {
            guard case RealmDatabaseImpl.DatabaseError.unsupportedIdType = error else {
                XCTFail("Invalid error type")
                return
            }
        }
        do {
            _ = try database.update(PersonDBEntity.self, id: invalidId, changedProperty: \PersonDBEntity.db_age, value: 12).toBlocking().first()
            XCTFail("No error emitted")
        } catch let error {
            guard case RealmDatabaseImpl.DatabaseError.unsupportedIdType = error else {
                XCTFail("Invalid error type")
                return
            }
        }
    }
}

