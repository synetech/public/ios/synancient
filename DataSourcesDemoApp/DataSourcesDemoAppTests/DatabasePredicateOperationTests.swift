//
//  DatabasePredicateOperationTests.swift
//  DataSourcesDemoAppTests
//
//  Created by Vojtěch Pajer on 14/11/2019.
//  Copyright © 2019 Vojtěch Pajer. All rights reserved.
//

import SYNDatabase
import RxBlocking
import RxCocoa
import RxSwift
import XCTest

@testable import DataSourcesDemoApp

final class DatabasePredicateOperationTests: XCTestCase {
    
    var databaseInstance: Database {
        return RealmDatabaseImpl(databaseSchemaVersion: 1, migrationBlock: nil, inMemoryIdentifier: UUID().uuidString)
    }
    let disposeBag = DisposeBag()
    
    func testGetFirst_Single() {
        let database = databaseInstance
        let queriedName = "Kristyna"
        let people = [
            PersonDBEntity(name: queriedName, age: 64), PersonDBEntity(name: queriedName, age: 82),
            PersonDBEntity(name: "Mirek", age: 2), PersonDBEntity(name: "Jirka", age: 18),
        ]
        _ = try! database.addAll(people).toBlocking().first()
        let person = try! database.getFirstOnce(PersonDBEntity.self,
                                                predicate: NSPredicate(format: "db_fullName = %@",
                                                                       queriedName))
            .toBlocking().first()
        XCTAssertEqual(person?.db_fullName, queriedName)
    }
    
    func testGetFirst_Observable() {
        let database = databaseInstance
        let queriedName = "Kristyna"
        let people = [
            PersonDBEntity(name: queriedName, age: 64), PersonDBEntity(name: "Jozin", age: 82),
            PersonDBEntity(name: "Mirek", age: 2), PersonDBEntity(name: "Jirka", age: 18),
        ]
        let expectation = XCTestExpectation(description: "Event triggered")
        let personId = people.first(where: { $0.db_fullName == queriedName })?.db_id ?? ""
        
        _ = try! database.addAll(people).toBlocking().first()
        database.getFirst(PersonDBEntity.self,
                          predicate: NSPredicate(format: "db_fullName = %@",
                                                 queriedName))
            .subscribe(onNext: { person in
                expectation.fulfill()
            })
            .disposed(by: disposeBag)
        _ = try! database.update(PersonDBEntity.self, id: personId,
                                 changedProperty: \PersonDBEntity.db_sex,
                                 value: "Rarely").toBlocking().first()
        wait(for: [expectation], timeout: 2)
    }
    
    func testGetWithPredicate_Single() {
        let database = databaseInstance
        let queriedName = "Kristyna"
        let people = [
            PersonDBEntity(name: queriedName, age: 64), PersonDBEntity(name: queriedName, age: 82),
            PersonDBEntity(name: queriedName, age: 2), PersonDBEntity(name: "Jirka", age: 18),
        ]
        
        _ = try! database.addAll(people).toBlocking().first()
        let matchingPeople = try! database.getOnceWithPredicate(PersonDBEntity.self,
                                                                predicate: NSPredicate(format: "db_fullName = %@",
                                                                                       queriedName))
            .toBlocking().first()
        XCTAssertEqual(matchingPeople?.count, people.filter { $0.db_fullName == queriedName }.count )
    }
    
    func testGetWithPredicate_Observable() {
        let database = databaseInstance
        let queriedName = "Kristyna"
        let people = [
            PersonDBEntity(name: queriedName, age: 64), PersonDBEntity(name: queriedName, age: 82),
            PersonDBEntity(name: queriedName, age: 2), PersonDBEntity(name: "Jirka", age: 18),
        ]
        let expectation = XCTestExpectation(description: "Event triggered")
        
        _ = try! database.addAll(people).toBlocking().first()
        database.getWithPredicate(PersonDBEntity.self,
                                  predicate: NSPredicate(format: "db_fullName = %@",
                                                         queriedName))
            .subscribe(onNext: { matchingPeople in
                if matchingPeople.count == (people.filter { $0.db_fullName == queriedName }.count + 1) {
                    expectation.fulfill()
                }
            })
            .disposed(by: disposeBag)
        _ = try! database.add(PersonDBEntity(name: queriedName, age: 57)).toBlocking().first()
        
        wait(for: [expectation], timeout: 2)
    }
    
    func testGetWithPredicate_Observable_ObservablePredicate() {
        let database = databaseInstance
        let queriedName = "Kristyna"
        let queriedAge = 64
        let people = [
            PersonDBEntity(name: queriedName, age: queriedAge), PersonDBEntity(name: queriedName, age: 82),
            PersonDBEntity(name: queriedName, age: 2), PersonDBEntity(name: "Jirka", age: queriedAge),
        ]
        let expectationAdd = XCTestExpectation(description: "Event triggered on person add")
        let expectationChange = XCTestExpectation(description: "Event triggered on predicate change")
        let predicate = BehaviorRelay<NSPredicate>(value: NSPredicate(format: "db_fullName = %@",
                                                                      queriedName))
        
        _ = try! database.addAll(people).toBlocking().first()
        database.getWithPredicate(PersonDBEntity.self,
                                  predicate: predicate.asObservable())
            .subscribe(onNext: { matchingPeople in
                if matchingPeople.count == (people.filter { $0.db_fullName == queriedName }.count + 1) {
                    expectationAdd.fulfill()
                }
                if matchingPeople.count == 1 { expectationChange.fulfill() }
            })
            .disposed(by: disposeBag)
        _ = try! database.add(PersonDBEntity(name: queriedName, age: 57)).toBlocking().first()
        
        wait(for: [expectationAdd], timeout: 2)
        predicate.accept(NSPredicate(format: "db_fullName = %@ AND db_age = %d", queriedName, queriedAge))
        wait(for: [expectationChange], timeout: 2)
    }
    
    func testUpdateAllWithPredicate() {
        let database = databaseInstance
        let queriedName = "Kristyna"
        let newAgeValue = 50
        let newCityValue = "Praha"
        let people = [
            PersonDBEntity(name: queriedName, age: 40), PersonDBEntity(name: queriedName, age: 82),
            PersonDBEntity(name: queriedName, age: 2), PersonDBEntity(name: "Jirka", age: 12),
        ]
        let affectedPeopleCount = people.filter { $0.db_fullName == queriedName }.count
        let namePredicate = NSPredicate(format: "db_fullName = %@", queriedName)
        let agePredicate = NSPredicate(format: "db_age = %d", newAgeValue)
        let cityPredicate = NSPredicate(format: "db_city = %@", newCityValue)
        
        _ = try! database.addAll(people).toBlocking().first()
        _ = try! database.updateAllWithPredicate(PersonDBEntity.self, predicate: namePredicate,
                                                 updateBlock: { person in
                                                    person.db_age = newAgeValue
            }).toBlocking().first()
        
        _ = try! database.updateAllWithPredicate(PersonDBEntity.self, predicate: namePredicate,
                                                 changedProperty: \PersonDBEntity.db_city,
                                                 value: newCityValue)
            .toBlocking().first()
        
        let peopleWithNewAge = try! database.getWithPredicate(PersonDBEntity.self, predicate: agePredicate)
            .toBlocking().first()
        let peopleWithNewCity = try! database.getWithPredicate(PersonDBEntity.self, predicate: cityPredicate)
            .toBlocking().first()
        
        XCTAssertEqual(peopleWithNewAge?.count, affectedPeopleCount)
        XCTAssertEqual(peopleWithNewCity?.count, affectedPeopleCount)
    }
    
    func testDeleteWithPredicate() {
        let database = databaseInstance
        let queriedName = "Kristyna"
        let people = [
            PersonDBEntity(name: queriedName, age: 40), PersonDBEntity(name: queriedName, age: 82),
            PersonDBEntity(name: queriedName, age: 2), PersonDBEntity(name: "Jirka", age: 12),
        ]
        let expectedRemainingPeopleCount = people.filter { $0.db_fullName != queriedName }.count
        let predicate = NSPredicate(format: "db_fullName = %@", queriedName)
        _ = try! database.addAll(people).toBlocking().first()
        _ = try! database.deleteWithPredicate(PersonDBEntity.self, predicate: predicate)
            .toBlocking().first()
        let remainingPeople = try! database.getAll(PersonDBEntity.self).toBlocking().first()
        
        XCTAssertEqual(expectedRemainingPeopleCount, remainingPeople?.count)
    }
}
