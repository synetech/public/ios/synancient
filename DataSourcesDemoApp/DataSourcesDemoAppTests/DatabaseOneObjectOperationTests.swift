//
//  DatabaseOneObjectOperationTests.swift
//  DatabaseOneObjectOperationTests
//
//  Created by Vojtěch Pajer on 13/11/2019.
//  Copyright © 2019 Vojtěch Pajer. All rights reserved.
//

import XCTest
import RxBlocking
import SYNDatabase

@testable import DataSourcesDemoApp

final class DatabaseOneObjectOperationTests: XCTestCase {
    
    var databaseInstance: Database {
        return RealmDatabaseImpl(databaseSchemaVersion: 1, migrationBlock: nil, inMemoryIdentifier: UUID().uuidString)
    }
    
    func testGetOnce() {
        let database = databaseInstance
        let person = PersonDBEntity()
        let personId = person.db_id
        
        _ = try! database.add(person).toBlocking().first()
        
        let addedPerson = try! database.getOnce(PersonDBEntity.self, id: personId)
            .toBlocking()
            .first()
        
        XCTAssertEqual(addedPerson?.db_id, person.db_id)
        XCTAssertEqual(addedPerson?.db_fullName, person.db_fullName)
        XCTAssertEqual(addedPerson?.db_age, person.db_age)
        XCTAssertEqual(addedPerson?.db_birthday, person.db_birthday)
    }
    
    func testUpdateObject() {
        let database = databaseInstance
        let person = PersonDBEntity()
        let personId = person.db_id
        
        let newName = "Vaclav Troska"
        let newAge = 30
        
        _ = try! database.add(person).toBlocking().first()
        _ = try! database.update(person, updateBlock: { person in person.db_fullName = newName }).toBlocking().first()
        _ = try! database.update(person, changedProperty: \PersonDBEntity.db_age, value: newAge).toBlocking().first()
        
        let addedPerson = try! database.getOnce(PersonDBEntity.self, id: personId)
            .toBlocking()
            .first()
        
        XCTAssertEqual(addedPerson?.db_fullName, newName)
        XCTAssertEqual(addedPerson?.db_age, newAge)
    }
    
    func testUpdateObjectWithId() {
        let database = databaseInstance
        let person = PersonDBEntity()
        let personId = person.db_id
        
        let newBirthday = Date()
        let newCountry = "Norway"
        
        _ = try! database.add(person).toBlocking().first()
        _ = try! database.update(PersonDBEntity.self, id: personId,
                                 updateBlock: { person in person.db_country = newCountry }).toBlocking().first()
        _ = try! database.update(PersonDBEntity.self, id: personId,
                                 changedProperty: \PersonDBEntity.db_birthday, value: newBirthday).toBlocking().first()
        
        let addedPerson = try! database.getOnce(PersonDBEntity.self, id: personId)
            .toBlocking()
            .first()
        
        XCTAssertEqual(addedPerson?.db_birthday, newBirthday)
        XCTAssertEqual(addedPerson?.db_country, newCountry)
    }
    
    func testGetObervable() {
        let database = databaseInstance
        let person = PersonDBEntity()
        let personId = person.db_id
        let newName = "Ignac Priblizil"
        
        _ = try! database.add(person).toBlocking().first()
        
        let personObservable = database.get(PersonDBEntity.self, id: personId)
        let personInitialValue = try! personObservable.toBlocking().first()
        
        XCTAssertEqual(personInitialValue?.db_fullName, person.db_fullName)
        
        _ = try! database.update(person,
                                 updateBlock: { person in person.db_fullName = newName })
            .toBlocking().first()
        
        let personNewValue = try! personObservable.toBlocking().first()
        XCTAssertEqual(personNewValue?.db_fullName, newName)
    }
    
    func testGetFirst() {
        let database = databaseInstance
        let people = [
            PersonDBEntity(name: "Erika", age: 40), PersonDBEntity(name: "Tomas", age: 82),
            PersonDBEntity(name: "Igorka", age: 2), PersonDBEntity(name: "Jirka", age: 12),
        ]
        
        _ = try! database.addAll(people).toBlocking().first()
        guard let person = try! database.getFirst(PersonDBEntity.self, predicate: NSPredicate(value: true)).toBlocking().first() else {
            XCTAssert(false)
            return
        }
        XCTAssert(people.contains(person))
    }
}
